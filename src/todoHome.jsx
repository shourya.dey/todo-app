import React, { Component } from 'react';

class Todo extends Component {
    state = {
        todoLists: [],
    };

    render() {

        return (
            <div className="App">
                <h4>My Todos</h4>
                <div className="add-todo">
                    <label>Add/Edit Todo</label>
                    <input type="text" />
                    <button type="button">Add</button>
                </div>
                <div className="todo-list">
                    <ul>
                        <li></li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Todo;